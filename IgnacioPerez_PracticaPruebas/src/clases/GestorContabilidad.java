package clases;

import java.util.ArrayList;

public class GestorContabilidad {

	private ArrayList<Factura> listaFacturas;

	private ArrayList<Cliente> listaClientes;

	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public GestorContabilidad() {

		listaFacturas = new ArrayList<Factura>();

		listaClientes = new ArrayList<Cliente>();

	}

	public void altaCliente(Cliente cliente) {

		if (buscarCliente(cliente.getDni()) == null) {

			listaClientes.add(cliente);

		}

	}

	public void crearFactura(Factura factura1) {

		if (buscarFactura(factura1.getCodigoFactura()) == null) {

			listaFacturas.add(factura1);

		}

	}

	public Cliente buscarCliente(String dni) {

		if (!listaClientes.isEmpty()) {

			for (int i = 0; i < listaClientes.size(); i++) {

				if (listaClientes.get(i).getDni().equals(dni)) {

					return listaClientes.get(i);

				}

			}

			return null;

		}

		return null;

	}

	public Factura buscarFactura(String codigo) {

		if (!listaFacturas.isEmpty()) {

			for (int i = 0; i < listaFacturas.size(); i++) {

				if (listaFacturas.get(i).getCodigoFactura().equals(codigo)) {

					return listaFacturas.get(i);

				}

			}

			return null;

		}

		return null;

	}

	/**
	 * 
	 * @return
	 */

	public Cliente clienteMasAntiguo() {

		if (!listaClientes.isEmpty()) {

			Cliente clienteMasAntiguo = listaClientes.get(0);

			for (int i = 0; i < listaClientes.size(); i++) {

				if (listaClientes.get(i).getFechaAlta().isBefore(clienteMasAntiguo.getFechaAlta())) {

					clienteMasAntiguo = listaClientes.get(i);

				}

			}

			return clienteMasAntiguo;

		}

		return null;

	}

	public Factura facturaMasCara() {

		Factura facturaMasCara = null;

		if (!listaFacturas.isEmpty()) {

			facturaMasCara = listaFacturas.get(0);

			for (int i = 0; i < listaFacturas.size(); i++) {

				if (listaFacturas.get(i).calcularPrecioTotal() > facturaMasCara.calcularPrecioTotal()) {

					facturaMasCara = listaFacturas.get(i);

				}

			}

		}

		return facturaMasCara;

	}

	public float calcularFacturacionAnual(int anno) {

		float totalFacturacion = 0;

		if (!listaFacturas.isEmpty()) {

			for (Factura factura : listaFacturas) {

				if (factura.getFecha().getYear() == anno) {

					totalFacturacion = totalFacturacion + factura.calcularPrecioTotal();

				}

			}

			return totalFacturacion;

		}

		return totalFacturacion;

	}

	public void asignarClienteAFactura(String dni, String codigoFactura) {

		if (buscarCliente(dni) != null && buscarFactura(codigoFactura) != null) {

			buscarFactura(codigoFactura).setCliente(buscarCliente(dni));

		}

	}

	public float cantidadFacturasPorCliente(String dni) {

		float cantidadFacturas = 0;

		if (buscarCliente(dni) != null) {

			for (Factura factura : listaFacturas) {

				if (factura.getCliente() == buscarCliente(dni)) {

					cantidadFacturas++;
				}

			}
		}

		return cantidadFacturas;

	}

	public void eliminarFactura(String codigo) {

		if (buscarFactura(codigo) != null) {

			listaFacturas.remove(buscarFactura(codigo));

		}

	}

	public void eliminarCliente(String dni) {

		if (buscarCliente(dni) != null && clienteTieneFactura(dni) == false) {

			listaClientes.remove(buscarCliente(dni));

		}

	}

	public boolean clienteTieneFactura(String dni) {

		boolean confirmacion = false;

		for (int i = 0; i < listaFacturas.size(); i++) {

			if (listaFacturas.get(i).getCliente().getDni().equals(dni)) {

				confirmacion = true;

				break;

			}

		}

		return confirmacion;

	}

}
