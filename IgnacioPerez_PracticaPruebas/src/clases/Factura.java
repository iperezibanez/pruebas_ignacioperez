package clases;

import java.time.LocalDateTime;

public class Factura {

	private String codigoFactura;
	private LocalDateTime fecha;
	private String nombreProducto;
	private float precioUnidad;
	private int cantidad;
	private Cliente cliente;

	public String getCodigoFactura() {
		return codigoFactura;
	}

	public void setCodigoFactura(String codigoFactura) {
		this.codigoFactura = codigoFactura;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public float getPrecioUnidad() {
		return precioUnidad;
	}

	public void setPrecioUnidad(float precioUnidad) {
		this.precioUnidad = precioUnidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Factura(String codigoFactura, LocalDateTime fecha, String nombreProducto, float precioUnidad, int cantidad) {
		super();

		this.codigoFactura = codigoFactura;
		this.fecha = fecha;
		this.nombreProducto = nombreProducto;
		this.precioUnidad = precioUnidad;
		this.cantidad = cantidad;

	}

	public String toString() {

		return codigoFactura + " / " + cliente + " / " + cantidad + " / " + nombreProducto + " / " + precioUnidad
				+ " / " + fecha;

	}

	public float calcularPrecioTotal() {

		float precioTotal = cantidad * precioUnidad;

		return precioTotal;

	}

}
