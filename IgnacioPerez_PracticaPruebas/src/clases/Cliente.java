package clases;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Cliente implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1901906192300240903L;
	private String nombre;
	private String dni;
	private LocalDateTime fechaAlta;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public LocalDateTime getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(LocalDateTime fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Cliente(String nombre, String dni, LocalDateTime fechaAlta) {

		this.nombre = nombre;
		this.dni = dni;
		this.fechaAlta = fechaAlta;
	}

	public String toString() {

		return nombre + " / " + dni + " / " + fechaAlta;

	}

}
