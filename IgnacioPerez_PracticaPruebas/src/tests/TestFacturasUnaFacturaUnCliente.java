package tests;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.After;

import org.junit.Before;

import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestFacturasUnaFacturaUnCliente {

	private static final float DELTA = 1e-3F;
	GestorContabilidad nuevoGestor;
	Factura nuevaFactura;
	Cliente nuevoCliente;

	@Before
	public void setUp() throws Exception {

		nuevoGestor = new GestorContabilidad();

		nuevaFactura = new Factura("1A", LocalDateTime.of(2014, 4, 3, 13, 45, 55), "Divacel", 21.33F, 4);

		nuevoGestor.getListaFacturas().add(nuevaFactura);

		nuevoCliente = new Cliente("Pedro Salazar", "186756F", LocalDateTime.of(2015, 3, 21, 12, 00, 01));

		nuevoGestor.getListaClientes().add(nuevoCliente);
	}

	@After
	public void tearDown() throws Exception {

		nuevoGestor.getListaFacturas().clear();
		nuevoGestor.getListaClientes().clear();
	}

	/**
	 * Test que comprueba si el m�todo buscarFactura funciona en una listaFacturas
	 * en la que solo hay guardada una factura
	 */

	@Test
	public void buscarFacturaExisteUna() {

		assertSame(nuevaFactura, nuevoGestor.buscarFactura("1A"));

	}

	/**
	 * Test que comprueba si el m�todo buscarFactura responde de manera correcta
	 * cuando se le pasa un c�digo que no corresponde con ninguna factura guardada
	 * 
	 */

	@Test
	public void buscarFacturaCodigoNoRegistrado() {

		assertNull(nuevoGestor.buscarFactura("2A"));

	}

	/**
	 * Test que comprueba que el m�todo crearFactura no permite incorporar a la
	 * listaClientes una factura con un c�digo repetido
	 */

	@Test
	public void crearFacturaRepetida() {

		Factura nuevaFactura2 = new Factura("1A", LocalDateTime.of(2014, 4, 3, 13, 45, 55), "Divacel", 21.33F, 4);

		nuevoGestor.crearFactura(nuevaFactura2);

		assertFalse(nuevoGestor.getListaFacturas().contains(nuevaFactura2));

	}

	/**
	 * Test que comprueba que el m�todo crearFactura permite incorporar una factura
	 * con c�digo no repetido a la listaFacturas
	 */

	@Test
	public void crearFacturaNoRepetida() {

		Factura nuevaFactura2 = new Factura("1B", LocalDateTime.of(2017, 11, 3, 22, 45, 1), "Divacel", 27.76F, 4);

		nuevoGestor.crearFactura(nuevaFactura2);

		assertTrue(nuevoGestor.getListaFacturas().contains(nuevaFactura2));

	}

	/**
	 * Test que comprueba que el m�todo calcularFacturaci�nAnual devuelve cero
	 * cuando no se ha registrado ninguna factura en el a�o indicado
	 */

	@Test
	public void facturacionAnualNoExisteAno() {

		assertEquals(0, nuevoGestor.calcularFacturacionAnual(1990), DELTA);

	}

	/**
	 * Test que comprueba que el m�todo calcularFacturaci�nAnual funciona cuando, en
	 * el a�o indicado, solo se ha registrado una factura
	 */

	@Test
	public void facturacionAnualSoloUna() {

		assertEquals(85.32, nuevoGestor.calcularFacturacionAnual(2014), DELTA);

	}

	/**
	 * Test que comprueba que, cuando al m�todo asignarClienteFactura se le pasa un
	 * Dni desconocido, el apartado cliente de la factura se mantiene como null
	 */

	@Test
	public void asignarClienteFacturaClienteNoExiste() {

		nuevoGestor.asignarClienteAFactura("22222222B", "1A");

		assertNull(nuevaFactura.getCliente());

	}

	/**
	 * Test que comprueba que, cuando al m�todo asignarClienteFactura se le pasa un
	 * c�digo de factura desconocido, el apartado cliente de la factura se mantiene
	 * como null
	 */

	@Test
	public void asignarClienteFacturaFacturaNoExiste() {

		nuevoGestor.asignarClienteAFactura("186756F", "1B");

		assertNull(nuevaFactura.getCliente());

	}

	/**
	 * Test que comprueba que, cuando se le pasa un Dni y un c�digo de factura
	 * existentes, el m�todo asignarClienteFactura funciona de forma correcta
	 */

	@Test
	public void asignarClienteFacturaTodoCorrecto() {

		nuevoGestor.asignarClienteAFactura("186756F", "1A");

		assertSame(nuevoGestor.buscarCliente("186756F"), nuevaFactura.getCliente());

	}

	/**
	 * Test que comprueba que el m�todo cantidadFacturasCliente devuelve cero cuando
	 * se le pasa el dni de una persona que todav�a no ha sido registrada en la
	 * aplicaci�n
	 */

	@Test
	public void cantidadFacturasClienteNoExisteCliente() {

		assertEquals(0, nuevoGestor.cantidadFacturasPorCliente("00000000F"), DELTA);

	}

	/**
	 * Test que comprueba que el m�todo eliminarFactura no realiza ninguna acci�n
	 * cuando se le pasa el c�digo de una factura que no existe
	 */

	@Test
	public void eliminarFacturaNoExistente() {

		nuevoGestor.eliminarFactura("1B");

		assertEquals(1, nuevoGestor.getListaFacturas().size());

	}

	/**
	 * Test que comprueba que el m�todo eliminarFactura funciona de manera correcta
	 * cuando solo hay una factura registrada
	 */

	@Test
	public void eliminarFacturaExisteUna() {

		nuevoGestor.eliminarFactura("1A");

		assertEquals(0, nuevoGestor.getListaFacturas().size());

	}

	/**
	 * Test que comprueba si el m�todo asignarClienteFactura, encadenando varios
	 * cambios de cliente --incluso Dni's no registrados--, mantiene la �ltima
	 * versi�n del cliente asociado a la factura
	 */

	@Test
	public void asignarClienteFacturaRealizandoCambios() {

		Cliente nuevoCliente2 = new Cliente("Ram�n Sampedro", "32617346T", LocalDateTime.of(2015, 3, 21, 12, 00, 01));

		nuevoGestor.getListaClientes().add(nuevoCliente2);

		nuevoGestor.asignarClienteAFactura("186756F", "1A");
		nuevoGestor.asignarClienteAFactura("0000000000T", "1A");
		nuevoGestor.asignarClienteAFactura("32617346T", "1A");

		assertSame(nuevoCliente2, nuevaFactura.getCliente());

	}

}
