package tests;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.After;

import org.junit.Before;

import org.junit.Test;

import clases.Cliente;
import clases.GestorContabilidad;

public class TestClientesUnElemento {

	GestorContabilidad nuevoGestor;
	Cliente nuevoCliente1;

	@Before
	public void setUp() throws Exception {

		nuevoGestor = new GestorContabilidad();

		nuevoCliente1 = new Cliente("Pedro Salazar", "186756F", LocalDateTime.of(2015, 3, 21, 12, 00, 01));

		nuevoGestor.getListaClientes().add(nuevoCliente1);

	}

	@After
	public void tearDown() throws Exception {

		nuevoGestor.getListaClientes().clear();
	}

	/**
	 * Test que prueba que el m�todo buscarCliente funciona cuando en el ArrayList
	 * listaClientes solo hay guardado un cliente
	 * 
	 */

	@Test
	public void buscarClienteExisteUno() {

		assertSame(nuevoCliente1, nuevoGestor.buscarCliente("186756F"));

	}

	/**
	 * Test que prueba que el m�todo buscarCliente devuelve null cuando el dni
	 * introducido no est� registrado en el ArrayList listaClientes
	 */

	@Test
	public void buscarClienteDniNoRegistrado() {

		assertNull(nuevoGestor.buscarCliente("222222F"));

	}

	/**
	 * Test que prueba que el m�todo altaCliente permite incorporar a
	 * listaClientes un cliente con Dni diferente al que ya ten�amos
	 */

	@Test
	public void altaClienteDiferenteDni() {

		Cliente nuevoCliente2 = new Cliente("Juan P�rez", "18171645E", LocalDateTime.of(2011, 6, 22, 1, 37, 01));

		nuevoGestor.altaCliente(nuevoCliente2);

		assertTrue(nuevoGestor.getListaClientes().contains(nuevoCliente2));

	}

	/**
	 * Test que comprueba que el m�todo altaCliente no incluye en la listaClientes
	 * elementos con Dni's iguales
	 */

	@Test
	public void altaClienteIgualDni() {

		Cliente nuevoCliente2 = new Cliente("Rafael N��ez", "186756F", LocalDateTime.of(2015, 3, 21, 12, 00, 01));

		nuevoGestor.altaCliente(nuevoCliente2);

		assertFalse(nuevoGestor.getListaClientes().contains(nuevoCliente2));

	}

	/**
	 * Test que comprueba que el m�todo altaCliente funciona cuando se introduce un
	 * cliente con un dni que ya existe y otro cliente con un dni diferente
	 */

	@Test
	public void altaClienteCombinarDiferenteIgualDni() {

		Cliente nuevoCliente2 = new Cliente("Ra�l Ib��ez", "186756F", LocalDateTime.of(2015, 3, 21, 12, 00, 01));
		Cliente nuevoCliente3 = new Cliente("Ignacio Perez", "18171645E", LocalDateTime.of(2015, 3, 21, 12, 00, 01));

		nuevoGestor.altaCliente(nuevoCliente2);
		nuevoGestor.altaCliente(nuevoCliente3);

		assertEquals(2, nuevoGestor.getListaClientes().size());

	}

	/**
	 * Test que comprueba que, con un solo cliente en listaClientes, el m�todo
	 * clienteMasAntiguo devuelve ese mismo cliente
	 */

	@Test
	public void clienteMasAntiguoUnico() {

		assertSame(nuevoCliente1, nuevoGestor.clienteMasAntiguo());

	}

	/**
	 * Test que comprueba que el m�todo eliminarCliente borra de listaClientes a un
	 * cliente cuyo Dni no est� asociado a ninguna factura
	 * 
	 */

	@Test
	public void eliminarClienteSinFactura() {

		nuevoGestor.eliminarCliente("186756F");

		assertFalse(nuevoGestor.getListaClientes().contains(nuevoCliente1));

	}

	/**
	 * Test que comprueba que el m�todo eliminarCliente no realiza ninguna acci�n
	 * cuando se le pasa un dni que no corresponde a ning�n cliente
	 */

	@Test
	public void eliminarClienteDniNoRegistrado() {

		nuevoGestor.eliminarCliente("11111111F");

		assertEquals(1, nuevoGestor.getListaClientes().size());

	}

}
