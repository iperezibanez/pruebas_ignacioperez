package tests;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.GestorContabilidad;

public class TestClientesTresElementos {

	static GestorContabilidad nuevoGestor;
	static Cliente nuevoCliente1;
	static Cliente nuevoCliente2;
	static Cliente nuevoCliente3;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		nuevoGestor = new GestorContabilidad();

		nuevoCliente1 = new Cliente("Pedro Salazar", "186756F", LocalDateTime.of(2015, 3, 21, 12, 00, 00));

		nuevoCliente2 = new Cliente("Ignacio Perez", "11111111H", LocalDateTime.of(2015, 3, 21, 11, 59, 00));

		nuevoCliente3 = new Cliente("Mar�a Rubido", "31167167H", LocalDateTime.of(2016, 3, 11, 11, 00, 00));

		nuevoGestor.getListaClientes().add(nuevoCliente1);

		nuevoGestor.getListaClientes().add(nuevoCliente2);

		nuevoGestor.getListaClientes().add(nuevoCliente3);

	}

	/**
	 * Test que pone a prueba el m�todo clienteMasAntiguo cuando la diferencia entre
	 * las fechas de alta de dos Clientes es de un segundo
	 */

	@Test
	public void clienteMasAntiguoDosDiferenciaSegundos() {

		assertSame(nuevoCliente2, nuevoGestor.clienteMasAntiguo());

	}

	/**
	 * Test que prueba que el m�todo buscarCliente funciona cuando hay varios
	 * clientes guardados en el ArrayList ListaClientes
	 */

	@Test
	public void buscarClienteExistenVarios() {

		assertSame(nuevoCliente3, nuevoGestor.buscarCliente("31167167H"));

	}

}
