package tests;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.After;

import org.junit.Before;

import org.junit.Test;

import clases.Factura;
import clases.GestorContabilidad;

public class TresFacturasTresElementos {

	private static final double DELTA = 1e-3F;
	GestorContabilidad nuevoGestor;
	Factura nuevaFactura1;
	Factura nuevaFactura2;
	Factura nuevaFactura3;

	@Before
	public void setUp() throws Exception {

		nuevoGestor = new GestorContabilidad();

		nuevaFactura1 = new Factura("1A", LocalDateTime.of(2014, 4, 3, 13, 45, 55), "Divacel", 21.33F, 4);
		nuevaFactura2 = new Factura("1B", LocalDateTime.of(1992, 2, 12, 7, 8), "Caramelo", 21.33F, 4);
		nuevaFactura3 = new Factura("1C", LocalDateTime.of(1992, 2, 12, 7, 8), "Caballos", 20F, 1);

		nuevoGestor.getListaFacturas().add(nuevaFactura1);
		nuevoGestor.getListaFacturas().add(nuevaFactura2);
		nuevoGestor.getListaFacturas().add(nuevaFactura3);

	}

	@After
	public void tearDown() throws Exception {
		
		nuevoGestor.getListaFacturas().clear();
	}

	/**
	 * Test que comprueba si el m�todo buscarFactura funciona cuando en
	 * listaFacturas hay varios elementos
	 */

	@Test
	public void buscarFacturaVariasFacturas() {

		assertSame(nuevaFactura2, nuevoGestor.buscarFactura("1B"));

	}

	/**
	 * Test que comprueba que, si el ArrayList listaFacturas contiene dos facturas
	 * que suman lo mismo, el m�todo facturaMasCara devolver� la factura que est�
	 * posicionada antes en el ArrayList
	 */

	@Test
	public void facturaMasCaraDosIguales() {

		assertSame(nuevaFactura1, nuevoGestor.facturaMasCara());

	}

	/**
	 * Test que comprueba que, cuando listaFacturas est� formada por m�s de dos
	 * facturas y una de ellas es m�s cara que las dem�s, el m�todo facturaMasCara
	 * devuelve ese elemento
	 */

	@Test
	public void facturaMasCaraDiferente() {

		Factura nuevaFactura4 = new Factura("1D", LocalDateTime.of(2014, 4, 3, 13, 45, 55), "Eritromicina", 21.33F, 5);

		nuevoGestor.getListaFacturas().add(nuevaFactura4);

		assertSame(nuevaFactura4, nuevoGestor.facturaMasCara());

	}

	/**
	 * Test que comprueba que el m�todo calcularFacturacionAnual funciona cuando, en
	 * listaFacturas, hay m�s de una factura correspondiente al a�o buscado
	 */

	@Test
	public void facturacionAnualVariasAno() {

		assertEquals(105.32, nuevoGestor.calcularFacturacionAnual(1992), DELTA);

	}

	/**
	 * Test que comprueba que el m�todo calcularFacturacionAnual funciona cuando, en
	 * listaFacturas, solo hay una factura que correponde al a�o buscado
	 */

	@Test
	public void facturacionAnualUnicaAno() {

		assertEquals(85.32, nuevoGestor.calcularFacturacionAnual(2014), DELTA);

	}

	/**
	 * Test que comprueba que el m�todo eliminarFactura funciona de manera correcta
	 * cuando hay varias facturas registradas y escogemos una de ellas
	 */

	@Test
	public void eliminarFacturaVarias() {

		nuevoGestor.eliminarFactura("1C");

		assertFalse(nuevoGestor.getListaFacturas().contains(nuevaFactura3));

	}

}
