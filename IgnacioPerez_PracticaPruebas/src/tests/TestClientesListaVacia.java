package tests;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.BeforeClass;
import org.junit.Test;

import clases.Factura;
import clases.GestorContabilidad;

public class TestClientesListaVacia {

	private static final float DELTA = 1e-3F;
	static GestorContabilidad nuevoGestor;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		nuevoGestor = new GestorContabilidad();

	}

	/**
	 * Test que prueba que el m�todo buscarCliente devuelve null cuando la lista de
	 * clientes est� vac�a
	 */

	@Test
	public void buscarClienteListaVacia() {

		assertNull(nuevoGestor.buscarCliente("222222F"));

	}

	/**
	 * Test que comprueba que, teniendo una listaClientes vac�a, el m�todo
	 * clienteMasAntiguo devuelve null
	 */

	@Test
	public void clienteMasAntiguoVacio() {

		assertNull(nuevoGestor.clienteMasAntiguo());

	}

	/**
	 * Test que comprueba que el m�todo cantidadFacturasCliente devuelve cero cuando
	 * el ArrayList listaClientes est� vac�o
	 */

	@Test
	public void cantidadFacturasClienteListaClientesVacia() {

		Factura nuevaFactura1 = new Factura("1A", LocalDateTime.of(1992, 4, 5, 5, 5), "Pizza", 3F, 4);

		nuevoGestor.getListaFacturas().add(nuevaFactura1);

		nuevoGestor.asignarClienteAFactura("3", "1A");

		assertEquals(0, nuevoGestor.cantidadFacturasPorCliente("3"), DELTA);

	}

}
