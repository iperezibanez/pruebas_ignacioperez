package tests;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.After;

import org.junit.Before;

import org.junit.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestClienteAsociadoFactura {

	private static final float DELTA = 1e-3F;
	GestorContabilidad nuevoGestor;
	Cliente nuevoCliente1;
	Factura nuevaFactura1;
	Factura nuevaFactura2;

	@Before
	public void setUp() throws Exception {

		nuevoGestor = new GestorContabilidad();

		nuevaFactura1 = new Factura("1A", LocalDateTime.of(2014, 4, 3, 13, 45, 55), "Divacel", 21.33F, 4);

		nuevaFactura2 = new Factura("1B", LocalDateTime.of(2014, 4, 3, 13, 45, 55), "Divacel", 21.33F, 4);

		nuevoGestor.getListaFacturas().add(nuevaFactura1);

		nuevoGestor.getListaFacturas().add(nuevaFactura2);

		nuevoCliente1 = new Cliente("Pedro Salazar", "186756F", LocalDateTime.of(2015, 3, 21, 12, 00, 01));

		nuevoGestor.getListaClientes().add(nuevoCliente1);

		for (Factura factura1 : nuevoGestor.getListaFacturas()) {

			if (factura1.getCodigoFactura().equals("1A")) {

				factura1.setCliente(nuevoCliente1);

			} else if (factura1.getCodigoFactura().equals("1B")) {

				factura1.setCliente(nuevoCliente1);

			}

		}

	}

	@After
	public void tearDown() throws Exception {

		nuevoGestor.getListaClientes().clear();
		nuevoGestor.getListaFacturas().clear();
	}

	/**
	 * Test que comprueba que el m�todo cantidadFacturasCliente funciona de forma
	 * correcta cuando existe m�s de una factura asociada a la persona
	 */

	@Test
	public void cantidadFacturasClienteSinProblemas() {

		assertEquals(2, nuevoGestor.cantidadFacturasPorCliente("186756F"), DELTA);

	}

	/**
	 * Test que comprueba que el m�todo eliminarCliente no borra a clientes que
	 * tienen asociadas facturas
	 */

	@Test
	public void eliminarClienteConFactura() {

		nuevoGestor.eliminarCliente("186756F");

		assertTrue(nuevoGestor.getListaClientes().contains(nuevoCliente1));

	}

}
