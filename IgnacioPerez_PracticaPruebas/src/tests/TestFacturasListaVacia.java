package tests;

import static org.junit.Assert.*;

import java.time.LocalDateTime;

import org.junit.BeforeClass;
import org.junit.Test;

import clases.Cliente;
import clases.GestorContabilidad;

public class TestFacturasListaVacia {

	private static final float DELTA = 1e-3F;
	static GestorContabilidad nuevoGestor;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

		nuevoGestor = new GestorContabilidad();
	}

	/**
	 * Test que comprueba que el m�todo buscarFactura devuelve null cuando la lista
	 * de facturas est� vac�a
	 */

	@Test
	public void buscarFacturaListaVacia() {

		assertNull(nuevoGestor.buscarFactura("1A"));

	}

	/**
	 * Test que comprueba que el metodo facturaMasCara devuelve null cuando la
	 * listaFacturas est� vac�a
	 */

	@Test
	public void facturaMasCaraVacio() {

		assertNull(nuevoGestor.facturaMasCara());

	}

	/**
	 * Test que comprueba que el m�todo calcularFacturaci�nAnual devuelve cero
	 * cuando la lista de facturas est� vac�a
	 */

	@Test
	public void FacturacionAnualListaVacia() {

		assertEquals(0, nuevoGestor.calcularFacturacionAnual(1990), DELTA);

	}

	/**
	 * Test que comprueba que el m�todo cantidadFacturasCliente devuelve cero cuando
	 * el ArrayList listaFacturas est� vac�o
	 */

	@Test
	public void cantidadFacturasClienteListaFacturasVacia() {

		Cliente nuevoCliente1 = new Cliente("Rafael Ib��ez", "186756F", LocalDateTime.of(1994, 4, 3, 2, 1, 5));

		nuevoGestor.getListaClientes().add(nuevoCliente1);

		nuevoGestor.asignarClienteAFactura("186756F", "1D");

		assertEquals(0, nuevoGestor.cantidadFacturasPorCliente("186756F"), DELTA);

	}

}
